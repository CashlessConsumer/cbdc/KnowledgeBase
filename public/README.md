---
description: >-
  Citizen's guide to Reserve Bank of India's e₹ - Retail / Central Bank Digital
  Currency.
---

# 👋 e₹ - Retail - Dummies guide to Indian CBDC Pilot

{% hint style="danger" %}
**Disclaimer:** This is unofficial knowledge base of e₹-Retail. While care is taken to make information as accurate as possible, please verify.&#x20;
{% endhint %}

## Overview

Here are a couple of example overviews from products with really great docs:

> Loom is a video messaging tool that helps you get your message across through instantly shareable videos.
>
> With Loom, you can record your camera, microphone, and desktop simultaneously. Your video is then instantly available to share through Loom's patented technology.
>
> — From the [Loom Docs](https://support.loom.com/hc/en-us/articles/360002158057-What-is-Loom-)

> The Mailchimp Marketing API provides programmatic access to Mailchimp data and functionality, allowing developers to build custom features to do things like sync email activity and campaign analytics with their database, manage audiences and campaigns, and more.
>
> — From the [Mailchimp Marketing API docs](https://mailchimp.com/developer/marketing/docs/fundamentals/)

## Quick links

{% content-ref url="overview/what-it-is.md" %}
[what-it-is.md](overview/what-it-is.md)
{% endcontent-ref %}

{% content-ref url="overview/features.md" %}
[features.md](overview/features.md)
{% endcontent-ref %}

## Get Started

We've put together some helpful guides for you to get setup with our product quickly and easily.

{% content-ref url="fundamentals/getting-set-up/" %}
[getting-set-up](fundamentals/getting-set-up/)
{% endcontent-ref %}

{% content-ref url="fundamentals/getting-set-up/setting-permissions.md" %}
[setting-permissions.md](fundamentals/getting-set-up/setting-permissions.md)
{% endcontent-ref %}

{% content-ref url="fundamentals/getting-set-up/inviting-members.md" %}
[inviting-members.md](fundamentals/getting-set-up/inviting-members.md)
{% endcontent-ref %}
