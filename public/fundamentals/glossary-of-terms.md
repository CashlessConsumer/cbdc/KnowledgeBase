# 📖 Glossary of Terms

##

<table><thead><tr><th width="235"></th><th></th></tr></thead><tbody><tr><td>CBDC</td><td></td></tr><tr><td>eRupee / Digital Rupee</td><td></td></tr><tr><td></td><td></td></tr><tr><td></td><td></td></tr><tr><td>Linked Bank Account</td><td></td></tr><tr><td>Linked Mobile Number</td><td></td></tr><tr><td>Load</td><td></td></tr><tr><td></td><td></td></tr><tr><td>QR Code</td><td></td></tr><tr><td>Redeem</td><td></td></tr><tr><td>Token</td><td></td></tr><tr><td>Wallet</td><td></td></tr><tr><td>Wallet Address</td><td></td></tr><tr><td>Wallet PIN</td><td></td></tr><tr><td>Wallet Provider</td><td></td></tr><tr><td></td><td></td></tr><tr><td></td><td></td></tr></tbody></table>

## Step 2 - Create Post

Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Maecenas faucibus mollis interdum. Cras mattis consectetur purus sit amet fermentum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Donec ullamcorper nulla non metus auctor fringilla. Nullam quis risus eget urna mollis ornare vel eu leo. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.

![](https://images.unsplash.com/photo-1515378791036-0648a3ef77b2?crop=entropy\&cs=tinysrgb\&fm=jpg\&ixid=MnwxOTcwMjR8MHwxfHNlYXJjaHw2fHxwb3N0fGVufDB8fHx8MTY2MDU4ODAzMg\&ixlib=rb-1.2.1\&q=80)
