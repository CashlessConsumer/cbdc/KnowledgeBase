---
description: 10000 feet overview of what e₹-R is.
---

# ❓ What it is

## Video overview

How RBI introduces e₹-R in its 3 minute video.

{% embed url="https://www.youtube.com/watch?v=_DL32-Xw22A" %}
India's Central Bank Digital Currency – Retail (e₹-R) Pilot
{% endembed %}

## Textual Definition&#x20;

### What RBI Says

e₹-R (e Rupee Retail) is a [Central Bank Digital Currency](https://en.wikipedia.org/wiki/Central\_Bank\_Digital\_Currency) introduced in pilot mode by the Reserve Bank of India from 01 Dec 2022 ([Press Release](https://www.rbi.org.in/Scripts/BS\_PressReleaseDisplay.aspx?prid=54773)).

The e₹-R would be in the form of a digital token that represents legal tender. It would be issued in the same denominations that paper currency and coins are currently issued. It would be distributed through intermediaries, i.e., banks. Users will be able to transact with e₹-R through a digital wallet offered by the participating banks and stored on mobile phones / devices. Transactions can be both Person to Person (P2P) and Person to Merchant (P2M). Payments to merchants can be made using QR codes displayed at merchant locations. The e₹-R would offer features of physical cash like trust, safety and settlement finality. As in the case of cash, it will not earn any interest and can be converted to other forms of money, like deposits with banks.&#x20;

### Alternate definitions

e₹-R is a [stable coin](https://en.wikipedia.org/wiki/Stablecoin) backed by Reserve Bank of India, distributed by banks regulated by it to its customers via a wallet in the form of denominated tokens and all transactions are logged between wallet holders processed via a private blockchain operated by [wallet providers ](../technology/wallet-providers.md)(banks), [National Payments Corporation of India](https://en.wikipedia.org/wiki/National\_Payments\_Corporation\_of\_India) and the Reserve Bank of India.

The digital token represents a legal tender - and is not a legal tender by itself as the e₹-R is still in pilot mode. e₹-R differs from cash as it does not provide even counter-party anonymity as transacting parties (and their respective wallet providers) share the KYC names of transacting parties besides having a central store of transaction log on the private blockchain.

