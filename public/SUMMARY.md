# Table of contents

* [👋 e₹ - Retail - Dummies guide to Indian CBDC Pilot](README.md)

## Overview

* [❓ What it is](overview/what-it-is.md)
* [✨ Features](overview/features.md)
* [🛑 Anti Features](overview/anti-features.md)

## Fundamentals

* [📖 Glossary of Terms](fundamentals/glossary-of-terms.md)
* [🛠 Getting set up](fundamentals/getting-set-up/README.md)
  * [📝 Setting permissions](fundamentals/getting-set-up/setting-permissions.md)
  * [🧑 Inviting Members](fundamentals/getting-set-up/inviting-members.md)

## Technology

* [📪 Design](technology/design.md)
* [📎 Transaction Flow](technology/transaction-flow.md)
* [📎 Wallet Providers](technology/wallet-providers.md)

## policy

* [🌍 Global](policy/global.md)
* [📎 Transaction Flow](policy/transaction-flow.md)

## RBI SAYS

* [🎨 Regulations / Policies](rbi-says/regulations-policies.md)
* [🎨 Media reports](rbi-says/media-reports.md)
* [🖥 Data](rbi-says/data.md)
